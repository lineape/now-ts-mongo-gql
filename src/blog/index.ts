import { getModelForClass, mongoose } from '@typegoose/typegoose';
import { gql } from 'apollo-server-micro';
import { BlogPost } from './schemas/blog-post.schema';
import { BlogAuthor } from './schemas/blog-author.schema';
import { createBlogPostResolvers } from './blog-post';
import { createBlogAuthorResolvers } from './blog-author';

export type GetAuthorModel = () => mongoose.Model<
  BlogAuthor & mongoose.Document
>;
export type GetPostModel = () => mongoose.Model<BlogPost & mongoose.Document>;

export const blogTypeDefs = gql`
  extend type Query {
    blogGetAuthors: [BlogAuthor!]!
    blogGetPosts: [BlogPost!]!

    blogFindAuthorById(id: String!): BlogAuthor
    blogFindAuthorBySlug(slug: String!): BlogAuthor
    blogFindPostById(id: String!): BlogPost
    blogFindPostBySlug(slug: String!): BlogPost
  }

  extend type Mutation {
    blogCreateAuthor(name: String!, slug: String!): BlogAuthor
    blogCreatePost(
      authorId: String!
      slug: String!
      title: String!
      body: String!
    ): BlogPost

    blogUpdateAuthorById(id: String!, name: String, slug: String): BlogAuthor
    blogUpdatePostById(
      id: String!
      slug: String
      title: String
      body: String
    ): BlogPost

    blogDeleteAuthorById(id: String!): Boolean!
    blogDeletePostById(id: String!): Boolean!
  }

  type BlogPost {
    _id: String!
    author: BlogAuthor
    body: String!
    slug: String!
    title: String!
  }

  type BlogAuthor {
    _id: String!
    name: String!
    posts: [BlogPost!]!
    slug: String!
  }
`;

export function createBlogResolvers(
  AuthorModel = getModelForClass(BlogAuthor),
  PostModel = getModelForClass(BlogPost),
) {
  const getAuthorModel = () => AuthorModel;
  const getPostModel = () => PostModel;

  const blogAuthorResolvers = createBlogAuthorResolvers(
    getAuthorModel,
    getPostModel,
  );
  const blogPostResolvers = createBlogPostResolvers(
    getAuthorModel,
    getPostModel,
  );

  return {
    Query: {
      ...blogAuthorResolvers.Query,
      ...blogPostResolvers.Query,
    },

    BlogPost: blogPostResolvers.BlogPost,

    BlogAuthor: blogAuthorResolvers.BlogAuthor,

    Mutation: {
      ...blogAuthorResolvers.Mutation,
      ...blogPostResolvers.Mutation,
    },
  };
}
