import { isDocumentArray, mongoose } from '@typegoose/typegoose';
import { GraphQLFieldResolver } from 'graphql';
import { GetAuthorModel, GetPostModel } from '.';
import { BlogAuthor } from './schemas/blog-author.schema';
import { BlogPost } from './schemas/blog-post.schema';

export const createBlogAuthorResolvers = (
  getAuthorModel: GetAuthorModel,
  getPostModel: GetPostModel,
) => ({
  Query: {
    blogGetAuthors: createBlogGetAuthorsResolver(getAuthorModel),
    blogFindAuthorById: createBlogFindAuthorByIdResolver(getAuthorModel),
    blogFindAuthorBySlug: createBlogFindAuthorBySlugResolver(getAuthorModel),
  },

  BlogAuthor: {
    posts: createBlogAuthorPostsResolver(),
  },

  Mutation: {
    blogCreateAuthor: createBlogCreateAuthorMutator(getAuthorModel),
    blogUpdateAuthorById: createBlogUpdateAuthorByIdMutator(getAuthorModel),
    blogDeleteAuthorById: createBlogDeleteAuthorByIdMutator(
      getAuthorModel,
      getPostModel,
    ),
  },
});

//
// Query resolvers
//
const createBlogGetAuthorsResolver = (
  getAuthorModel: GetAuthorModel,
): GraphQLFieldResolver<any, any> => (): Promise<BlogAuthor[]> =>
  getAuthorModel()
    .find()
    .exec();

const createBlogFindAuthorByIdResolver = (
  getAuthorModel: GetAuthorModel,
): GraphQLFieldResolver<any, any, { id: string }> => (
  _,
  { id },
): Promise<BlogAuthor | null> =>
  getAuthorModel()
    .findById(id)
    .exec();

const createBlogFindAuthorBySlugResolver = (
  getAuthorModel: GetAuthorModel,
): GraphQLFieldResolver<any, any, { slug: string }> => (
  _,
  { slug },
): Promise<BlogAuthor | null> =>
  getAuthorModel()
    .findOne({ slug })
    .exec();

//
// Field Resolvers
//
const createBlogAuthorPostsResolver = (): GraphQLFieldResolver<
  BlogAuthor & mongoose.Document,
  any
> => (author): Promise<BlogPost[]> =>
  author
    .populate('posts')
    .execPopulate()
    .then(() => (isDocumentArray(author.posts) ? author.posts : []));

//
// Mutators
//
const createBlogCreateAuthorMutator = (
  getAuthorModel: GetAuthorModel,
): GraphQLFieldResolver<any, any, { name: string; slug: string }> => (
  _,
  { name, slug },
): Promise<BlogAuthor | void> =>
  getAuthorModel()
    .create({ name, slug, posts: [] })
    .catch(console.error);

const createBlogUpdateAuthorByIdMutator = (
  getAuthorModel: GetAuthorModel,
): GraphQLFieldResolver<
  any,
  any,
  { id: string; name?: string; slug?: string }
> => async (_, { id, name, slug }): Promise<BlogAuthor | void> => {
  try {
    const author = await getAuthorModel()
      .findById(id)
      .exec();
    if (!author) return;

    author.name = name || author.name;
    author.slug = slug || author.slug;

    return await author.save();
  } catch (e) {
    console.error(e);
  }
};

const createBlogDeleteAuthorByIdMutator = (
  getAuthorModel: GetAuthorModel,
  getPostModel: GetPostModel,
): GraphQLFieldResolver<any, any, { id: string }> => async (
  _,
  { id },
): Promise<boolean> => {
  const session = await getAuthorModel().db.startSession();

  session.startTransaction();

  try {
    const author = await getAuthorModel()
      .findByIdAndDelete(id)
      .session(session)
      .exec();
    if (!author) {
      await session.abortTransaction();

      return false;
    }

    for (const postId of author.posts) {
      await getPostModel()
        .findByIdAndDelete(postId)
        .session(session)
        .exec();
    }

    await session.commitTransaction();

    return true;
  } catch (e) {
    await session.abortTransaction();

    return false;
  }
};
