import { prop, Ref } from '@typegoose/typegoose';
import { BlogAuthor, IBlogAuthor } from './blog-author.schema';

export interface IBlogPost {
  title: string;
  body: string;
  slug: string;
  author: IBlogAuthor;
}

export class BlogPost implements Omit<IBlogPost, 'author'> {
  @prop({ required: true })
  title!: string;

  @prop({ required: true, unique: true })
  slug!: string;

  @prop({ required: true })
  body!: string;

  @prop({ ref: 'BlogAuthor', required: true })
  author!: Ref<BlogAuthor>;
}
