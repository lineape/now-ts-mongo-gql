import { arrayProp, prop, Ref } from '@typegoose/typegoose';
import { BlogPost, IBlogPost } from './blog-post.schema';

export interface IBlogAuthor {
  name: string;
  slug: string;
  posts: IBlogPost[];
}

export class BlogAuthor implements Omit<IBlogAuthor, 'posts'> {
  @prop({ required: true })
  name!: string;

  @prop({ required: true, unique: true })
  slug!: string;

  @arrayProp({
    ref: 'BlogPost',
    localField: '_id',
    foreignField: 'author',
  })
  posts!: Array<Ref<BlogPost>>;
}
