import { isDocument, mongoose } from '@typegoose/typegoose';
import { GraphQLFieldResolver } from 'graphql';
import { GetAuthorModel, GetPostModel } from '.';
import { BlogPost, IBlogPost } from './schemas/blog-post.schema';
import { BlogAuthor } from './schemas/blog-author.schema';

export const createBlogPostResolvers = (
  getAuthorModel: GetAuthorModel,
  getPostModel: GetPostModel,
) => ({
  Query: {
    blogGetPosts: createBlogGetPostsResolver(getPostModel),
    blogFindPostById: createBlogFindPostByIdResolver(getPostModel),
    blogFindPostBySlug: createBlogFindPostBySlugResolver(getPostModel),
  },

  BlogPost: {
    author: createBlogPostAuthorResolver(),
  },

  Mutation: {
    blogCreatePost: createBlogCreatePostMutator(getAuthorModel, getPostModel),
    blogUpdatePostById: createBlogUpdatePostByIdMutator(getPostModel),
    blogDeletePostById: createBlogDeletePostByIdMutator(getPostModel),
  },
});

//
// Query resolvers
//
const createBlogGetPostsResolver = (
  getPostModel: GetPostModel,
): GraphQLFieldResolver<any, any> => (): Promise<BlogPost[]> =>
  getPostModel()
    .find()
    .exec();

const createBlogFindPostByIdResolver = (
  getPostModel: GetPostModel,
): GraphQLFieldResolver<any, any, { id: string }> => (
  _,
  { id },
): Promise<BlogPost | null> =>
  getPostModel()
    .findById(id)
    .exec();

const createBlogFindPostBySlugResolver = (
  getPostModel: GetPostModel,
): GraphQLFieldResolver<any, any, { slug: string }> => (
  _,
  { slug },
): Promise<BlogPost | null> =>
  getPostModel()
    .findOne({ slug })
    .exec();

//
// Field Resolvers
//
const createBlogPostAuthorResolver = (): GraphQLFieldResolver<
  BlogPost & mongoose.Document,
  any
> => (post): Promise<BlogAuthor | null> =>
  post
    .populate('author')
    .execPopulate()
    .then(() => (isDocument(post.author) ? post.author : null));

//
// Mutators
//
const createBlogCreatePostMutator = (
  getAuthorModel: GetAuthorModel,
  getPostModel: GetPostModel,
): GraphQLFieldResolver<
  any,
  any,
  { authorId: string; title: string; body: string; slug: string }
> => async (_, { title, authorId, body, slug }): Promise<BlogPost | void> => {
  try {
    const author = await getAuthorModel()
      .findById(authorId)
      .exec();
    if (!author) return;

    const postObj: IBlogPost = { title, body, slug, author: author._id };

    return await getPostModel().create(postObj);
  } catch (e) {
    console.error(e);
  }
};

const createBlogUpdatePostByIdMutator = (
  getPostModel: GetPostModel,
): GraphQLFieldResolver<
  any,
  any,
  { id: string; title?: string; body?: string; slug?: string }
> => async (_, { id, title, body, slug }): Promise<BlogPost | void> => {
  try {
    const post = await getPostModel()
      .findById(id)
      .exec();
    if (!post) return;

    post.title = title || post.title;
    post.body = body || post.body;
    post.slug = slug || post.slug;

    return await post.save();
  } catch (e) {
    console.error(e);
  }
};

const createBlogDeletePostByIdMutator = (
  getPostModel: GetPostModel,
): GraphQLFieldResolver<any, any, { id: string }> => async (
  _,
  { id },
): Promise<boolean> => {
  try {
    const post = await getPostModel()
      .findByIdAndDelete(id)
      .exec();

    return !!post;
  } catch (e) {
    return false;
  }
};
