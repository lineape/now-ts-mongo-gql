import 'reflect-metadata';
import { ApolloServer, gql } from 'apollo-server-micro';
import { mongoose } from '@typegoose/typegoose';
import { blogTypeDefs, createBlogResolvers } from './blog';
import { NowRequest, NowResponse } from '@now/node/dist';

const serverStart = new Date();

const rootTypeDefs = gql`
  type Query {
    getServerStartTime: String!
  }

  type Mutation {
    noop: Boolean
  }
`;

mongoose.connect(process.env.MONGODB_URI || '', { useFindAndModify: false });

const apolloServer = new ApolloServer({
  typeDefs: [rootTypeDefs, blogTypeDefs],
  resolvers: [createRootResolvers(), createBlogResolvers()],
  playground: true,
  introspection: true,
});

function createRootResolvers() {
  return {
    Query: {
      getServerStartTime: () => serverStart.toString(),
    },
    Mutation: {
      noop: () => true,
    },
  };
}

const apolloHandler = apolloServer.createHandler();
export default async (req: NowRequest, res: NowResponse) => {
  return apolloHandler(req, res);
};
